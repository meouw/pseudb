<?php

namespace Meouw\Test\PseuDb;

use Meouw\PseuDb\EmptyTable;
use Meouw\PseuDb\Prototype\ArrayReader;
use Meouw\PseuDb\Row;
use PHPUnit\Framework\ExpectationFailedException;

class RowTest extends MockDbConnectionProvider
{
    /** @var EmptyTable */
    protected $table;
    /** @var  Row */
    protected $row;

    protected function setUp(): void
    {
        parent::setUp();

        $this->table = new EmptyTable(
            $this->cxn,
            'authors',
            new ArrayReader(__DIR__.'/proto'),
            new \DateTime()
        );

        $this->row = $this->table
            ->row(
                array(
                    'name' => 'Mary',
                    'age'  => 23
                )
            )
            ->getRow(array('id' => 1));
    }

    public function tearDown(): void
    {
        $this->table->tearDown();
    }

    public function testAssertFieldEquals_WhenPasses()
    {
        $this->row->assertFieldEquals('Mary', 'name');
    }

    public function testAssertFieldEquals_WhenFails()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldEquals('Paul', 'name');
    }

    public function testAssertFieldGreaterThan_WhenPasses()
    {
        $this->row->assertFieldGreaterThan(20, 'age');
    }

    public function testAssertFieldGreaterThan_WhenFails_Equal()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldGreaterThan(23, 'age');
    }

    public function testAssertFieldGreaterThan_WhenFails_IsLess()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldGreaterThan(24, 'age');
    }

    public function testAssertFieldGreaterThanOrEqual_WhenPasses()
    {
        $this->row->assertFieldGreaterThanOrEqual(20, 'age');
    }

    public function testAssertFieldGreaterThanOrEqual_WhenPasses_Equal()
    {
        $this->row->assertFieldGreaterThanOrEqual(23, 'age');
    }

    public function testAssertFieldGreaterThanOrEqual_WhenFails_IsLess()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldGreaterThanOrEqual(24, 'age');
    }

    public function testAssertFieldLessThan_WhenPasses()
    {
        $this->row->assertFieldLessThan(30, 'age');
    }

    public function testAssertFieldLessThan_WhenFails_Equal()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldLessThan(23, 'age');
    }

    public function testAssertFieldLessThan_WhenFails_IsGreater()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldLessThan(22, 'age');
    }

    public function testAssertFieldLessThanOrEqual_WhenPasses()
    {
        $this->row->assertFieldLessThanOrEqual(30, 'age');
    }

    public function testAssertFieldLessThanOrEqual_WhenPasses_Equal()
    {
        $this->row->assertFieldLessThanOrEqual(23, 'age');
    }

    public function testAssertFieldLessThanOrEqual_WhenFails_IsGreater()
    {
        $this->expectException(ExpectationFailedException::class);
        $this->row->assertFieldLessThanOrEqual(22, 'age');
    }

}