DROP TABLE IF EXISTS authors;
CREATE TABLE authors
(
  id   INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(100)        NOT NULL,
  age  TINYINT(3) UNSIGNED NOT NULL,
  born DATETIME NOT NULL,
  died DATETIME DEFAULT NULL
);

DROP TABLE IF EXISTS books;
CREATE TABLE books
(
  id        INT(10) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
  author_id INT(10) UNSIGNED NOT NULL,
  title     VARCHAR(100)     NOT NULL,
  CONSTRAINT books_authors_id_fk
  FOREIGN KEY (author_id) REFERENCES pseudb.authors (id)
);

CREATE INDEX books_authors_id_fk ON books (author_id);