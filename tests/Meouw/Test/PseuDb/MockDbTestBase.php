<?php
namespace Meouw\Test\PseuDb;

use Meouw\PseuDb\MockDb;

abstract class MockDbTestBase extends MockDbConnectionProvider
{
    /** @var  MockDb */
    protected $mdb;

    public function testTest()
    {
        $this->mdb->prepare(
            array(
                'authors' => 1
            )
        );

        $authors = $this->mdb->table('authors');
        $authors->assertRowCount(1);

        $row = $authors->getRow(array('id' => 1));
        $row->assertFieldEquals('Prototype Author Name', 'name');
    }
} 