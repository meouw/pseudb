<?php

namespace Meouw\Test\PseuDb;

use Meouw\PseuDb\Prototype\ArrayReader;
use Meouw\PseuDb\PseuDb;

class PseuDbTest extends MockDbTestBase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->mdb = new PseuDb(
            $this->cxn,
            new ArrayReader(__DIR__.'/proto'),
            new \DateTime()
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
} 