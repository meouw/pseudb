<?php

namespace Meouw\Test\PseuDb;

use Meouw\PseuDb\Prototype\ArrayReader;
use Meouw\PseuDb\EmptyTable;

class EmptyTableTest extends MockTableTestBase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->tbl = new EmptyTable(
            $this->cxn,
            'authors',
            new ArrayReader(__DIR__.'/proto'),
            new \DateTime()
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
} 