<?php
namespace Meouw\Test\PseuDb;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use PHPUnit\Framework\TestCase;

abstract class MockDbConnectionProvider extends TestCase
{
    /** @var  Connection */
    protected $cxn;

    protected function setUp(): void
    {
        $this->cxn = DriverManager::getConnection(
            array(
                'dbname'   => 'pseudb',
                'driver'   => 'pdo_mysql',
                'user'     => 'pseudb',
                'password' => 'pseudb',
                'host'     => 'localhost'
            )
        );
    }

    protected function tearDown(): void
    {
        $this->cxn->close();
    }
} 