<?php

namespace Meouw\Test\PseuDb;

use Meouw\PseuDb\Prototype\ArrayReader;
use Meouw\PseuDb\EmptyDb;

class EmptyDbTest extends MockDbTestBase
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->mdb = new EmptyDb(
            $this->cxn,
            new ArrayReader(__DIR__.'/proto'),
            new \DateTime()
        );
    }

    protected function tearDown(): void
    {
        parent::tearDown();
    }
} 