<?php
namespace Meouw\Test\PseuDb;

use Meouw\PseuDb\MockTable;

abstract class MockTableTestBase extends MockDbConnectionProvider
{
    /** @var  MockTable */
    protected $tbl;

    public function testTest()
    {
        $this->tbl->row(array('name' => 'William'));

        $this->tbl->assertRowCount(1);
    }
} 