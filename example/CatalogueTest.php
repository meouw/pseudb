<?php
require 'Catalogue.php';

use Doctrine\DBAL\DriverManager;
use Meouw\PseuDb\Prototype\ArrayReader;
use Meouw\PseuDb\PseuDb;
use PHPUnit\Framework\TestCase;

class CatalogueTest extends TestCase
{
    /** @var  Catalogue */
    protected $catalogue;
    /** @var  PseuDb */
    protected $pseudb;

    protected function setUp(): void
    {
        $pdo = new PDO('mysql:dbname=pseudb', 'pseudb', 'pseudb');
        $this->catalogue = new Catalogue($pdo);
        $this->pseudb = new PseuDb(
            DriverManager::getConnection(['pdo' => $pdo]), new ArrayReader(__DIR__.'/proto'), new DateTime()
        );
    }

    protected function tearDown(): void
    {
        $this->pseudb->tearDown();
    }

    public function testFetchAuthor()
    {
        // put a fixture in place, overriding a field from the prototype
        $this->pseudb->table('authors')->row(
            [
                'name' => 'Iain M Banks',
            ]
        )
        ;

        $author = $this->catalogue->fetchAuthor(1);
        $this->assertEquals('Iain M Banks', $author['name']);
    }

    public function testAddAuthor()
    {
        $authorTable = $this->pseudb->table('authors');

        $this->catalogue->addAuthor('Enid Blyton', 60);

        $authorTable->assertRowCount(1);
        $authorTable->assertRowExists(['name' => 'Enid Blyton']);

        $enidRow = $authorTable->getRow(['id' => 1]);
        $enidRow->assertFieldEquals('Enid Blyton', 'name');
        $enidRow->assertFieldEquals(60, 'age');
    }

    public function testDeleteAuthor()
    {
        $authorsTable = $this->pseudb->table('authors');
        $authorsTable->rows(3);

        $this->catalogue->deleteAuthor(2);
        $authorsTable->assertRowCount(2);
        $authorsTable->assertRowExists(['id' => 1]);
        $authorsTable->assertRowExists(['id' => 3]);
    }

    public function testFetchBooksByAuthorId()
    {
        $this->pseudb->prepare(
            [
                'authors' => 2,
                'books' => 3,
            ]
        );

        $books = $this->catalogue->fetchBooksByAuthorId(1);
        $this->assertCount(3, $books);
        foreach ($books as $book) {
            $this->assertEquals(1, $book['author_id']);
        }
    }

}
 