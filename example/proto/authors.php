<?php
return array(
	'id'   => null,
	'name' => 'Prototype Author Name',
    'age'  => 30,
    'born' => $fixtureDate->sub(new DateInterval('P30Y'))->format('Y-m-d'),
    'died' => null
);
