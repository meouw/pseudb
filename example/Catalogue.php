<?php

class Catalogue
{
    /** @var  PDO */
    private $cxn;

    public function __construct(PDO $cxn)
    {
        $this->cxn = $cxn;
    }

    public function addAuthor($name, $age)
    {
        $stmt = $this->cxn->prepare("
            INSERT INTO authors (name, age)
            VALUES (:name, :age)
        ");

        $stmt->bindParam(':name', $name, PDO::PARAM_STR, 45);
        $stmt->bindParam(':age',  $age,  PDO::PARAM_INT);

        $stmt->execute();

        return $this->cxn->lastInsertId();
    }

    public function fetchAuthor($id)
    {
        $stmt = $this->cxn->prepare("
            SELECT  name
                ,   age
            FROM    authors
            WHERE   id = :id
        ");

        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        if ($stmt->rowCount()) {
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }

        return null;
    }

    public function deleteAuthor($id)
    {
        $stmt = $this->cxn->prepare("
            DELETE FROM authors
            WHERE       id = :id
        ");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();

        // also delete all books
        $stmt = $this->cxn->prepare("
            DELETE FROM books
            WHERE       author_id = :id
        ");
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function addBook($title, $authorId)
    {
        $stmt = $this->cxn->prepare("
            INSERT INTO books (title, author_id)
            VALUES (:title, :author_id)
        ");

        $stmt->bindParam(':title',     $title,    PDO::PARAM_STR, 45);
        $stmt->bindParam(':author_id', $authorId, PDO::PARAM_INT);

        $stmt->execute();

        return $this->cxn->lastInsertId();
    }

    public function fetchBooksByAuthorId($authorId)
    {
        $stmt = $this->cxn->prepare("
            SELECT id
                ,  title
                ,  author_id
            FROM   books
            WHERE  author_id = :author_id
        ");
        $stmt->bindParam(':author_id', $authorId, PDO::PARAM_INT);
        $stmt->execute();

        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
} 