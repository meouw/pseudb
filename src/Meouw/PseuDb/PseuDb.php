<?php

namespace Meouw\PseuDb;

class PseuDb extends MockDb
{
    /**
     * @param $prototypeName
     *
     * @return MockTable
     */
    public function table($prototypeName)
    {
        return parent::createTable(PseuTable::class, $prototypeName);
    }
}
