<?php

namespace Meouw\PseuDb;

use DateTime;
use Doctrine\DBAL\Connection;
use Meouw\PseuDb\Prototype\Reader;

/**
 * Class EmptyTable
 * @package Meouw\PseuDb
 */
class EmptyTable extends MockTable
{
    public function __construct(Connection $cxn, $tableName, Reader $prototypeReader, DateTime $fixtureDate)
    {
        parent::__construct($cxn, $tableName, $prototypeReader, $fixtureDate);
        if (!$this->readonly) {
            $this->truncate();
        }
    }

    public function tearDown()
    {
        if (!$this->readonly) {
            $this->truncate();
        }
    }
}
