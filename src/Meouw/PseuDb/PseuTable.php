<?php
namespace Meouw\PseuDb;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Exception;
use Meouw\PseuDb\Prototype\Reader;

class PseuTable extends MockTable
{
    public function __construct(Connection $cxn, $tableName, Reader $prototypeReader, DateTime $fixtureDate)
    {
        parent::__construct($cxn, $tableName, $prototypeReader, $fixtureDate);
        if (!$this->readonly) {
            $this->createTemporaryTable();
        }
    }

    /**
     *
     */
    protected function createTemporaryTable()
    {
        $tempTableName = uniqid();
        try {
            // create a copy to copy
            $this->cxn->exec("CREATE TEMPORARY TABLE $tempTableName LIKE $this->tableName");
            // copy the copy to mask the original
            $this->cxn->exec("DROP TEMPORARY TABLE IF EXISTS $this->tableName");
            $this->cxn->exec("CREATE TEMPORARY TABLE $this->tableName LIKE $tempTableName");
            // drop the copy we copied
            $this->cxn->exec("DROP TEMPORARY TABLE $tempTableName");
        } catch (Exception $e) {
            static::fail($e->getMessage());
        }
    }

    /**
     * @throws DBALException
     */
    public function tearDown()
    {
        $this->cxn->exec('SET foreign_key_checks = 0');
        $this->cxn->exec("DROP TEMPORARY TABLE IF EXISTS `$this->tableName`");
        $this->cxn->exec('SET foreign_key_checks = 1');
    }
}
