<?php

namespace Meouw\PseuDb\Prototype;

interface Writer
{
    public function write($tableName, $prototype);
} 