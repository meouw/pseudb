<?php

namespace Meouw\PseuDb\Prototype;

use InvalidArgumentException;
use LogicException;

class ArrayReader implements Reader
{
    protected $dir;

    public function __construct($prototypeDirectory)
    {
        $prototypeDirectory = realpath($prototypeDirectory);
        if (!$prototypeDirectory) {
            throw new InvalidArgumentException("$prototypeDirectory path does not exist");
        }
        $this->dir = $prototypeDirectory;
    }

    function read($tableName, \DateTime $fixtureDate, $variant = null)
    {
        $prototypePath = "{$this->dir}/$tableName.php";
        if (!is_readable($prototypePath)) {
            throw new InvalidArgumentException("Table fixture does not exist: '$tableName'");
        }
        $data = require $prototypePath;

        // If a variant has been specified then grab it
        if ($variant) {
            if (isset($data[$variant]) && is_array($data[$variant])) {
                $data = $data[$variant];
            }
            else {
                throw new LogicException("Variant '$variant' doesn't exist in table fixture '$tableName'");
            }
        }

        // if variants exist but none specified, use the first one
        if (is_array(reset($data))) {
            $data = current($data);
        }

        return $data;
    }
}