<?php

namespace Meouw\PseuDb\Prototype;

use DateTime;

interface Reader
{
    function read($tableName, DateTime $fixtureDate, $variant = null);
} 