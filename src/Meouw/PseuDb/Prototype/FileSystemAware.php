<?php

namespace Meouw\PseuDb\Prototype;

use InvalidArgumentException;

class FileSystemAware
{
    protected $dir;

    public function __construct($prototypeDirectory)
    {
        $prototypeDirectory = realpath($prototypeDirectory);
        if (!$prototypeDirectory) {
            throw new InvalidArgumentException("$prototypeDirectory path does not exist");
        }
        $this->dir = $prototypeDirectory;
    }
} 