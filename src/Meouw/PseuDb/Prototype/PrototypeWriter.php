<?php

namespace Meouw\PseuDb\Prototype;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Schema\Table;

class PrototypeWriter
{
    /**
     * Creates the fixtures required to run
     *
     * @param \Doctrine\DBAL\Connection $cxn
     * @param Writer $writer
     * @param bool $overwrite
     */
    public static function createFixtureTables(Connection $cxn, Writer $writer, $overwrite = false)
    {
        $schema = $cxn->getSchemaManager()->createSchema();
        $tables = $schema->getTables();

        /** @var $table Table */
        /** @var $column Column */
        foreach ($tables as $table) {
            $fixture = [];
            $tableName = $table->getName();

            $columns = $table->getColumns();
            foreach ($columns as $column) {
                $fixture[$column->getName()] = $column->getDefault();
            }
            $writer->write($tableName, $fixture);
        }
    }
} 