<?php

namespace Meouw\PseuDb\Prototype;

class ArrayWriter extends FileSystemAware implements Writer
{
    public function write($tableName, $prototype)
    {
        $len = 0;
        foreach (array_keys($prototype) as $key) {
            $len = max($len, strlen($key));
        }
        $len += 2;
        $format = "\t%-{$len}s => %s,\n";

        $data = "<?php\nthrow new Exception('Fixture has not been prepared for use: $tableName');\n\nreturn array(\n";
        foreach ($prototype as $key => $val) {
            if (is_null($val)) {
                $val = 'null';
            }
            elseif (!is_numeric($val)) {
                $val = "'$val'";
            }

            $data .= sprintf($format, "'$key'", $val);
        }
        $data .= ");\n";

        file_put_contents($this->dir.'/'.$tableName.'.php', $data);
    }
} 