<?php

namespace Meouw\PseuDb;

use DateTime;
use Doctrine\DBAL\Connection;
use Meouw\PseuDb\Prototype\Reader;
use PHPUnit\Framework\Assert;

/**
 * Class MockDb
 * @package Meouw\PseuDb
 */
abstract class MockDb extends Assert
{
    /** @var Connection */
    protected $cxn;
    /** @var MockTable[] */
    protected $tables = [];
    /** @var Reader */
    protected $prototypeReader;
    /** @var DateTime */
    protected $fixtureDate;

    /**
     * MockDb constructor.
     * @param Connection $cxn
     * @param Reader $prototypeReader
     * @param DateTime $fixtureDate
     */
    public function __construct(Connection $cxn, Reader $prototypeReader, DateTime $fixtureDate)
    {
        $this->cxn = $cxn;
        $this->prototypeReader = $prototypeReader;
        $this->fixtureDate = $fixtureDate;
    }

    /**
     * @param $prototypeName
     *
     * @return MockTable
     */
    abstract public function table($prototypeName);

    /**
     * @param $type
     * @param $prototypeName
     * @return MockTable
     */
    protected function createTable($type, $prototypeName)
    {
        $name = $this->getName($prototypeName);
        if (!isset($this->tables[$name])) {
            $this->tables[$name] = new $type(
                $this->cxn, $prototypeName, $this->prototypeReader, $this->fixtureDate
            );
        }

        return $this->tables[$name];
    }

    /**
     * @param array $fixture
     */
    public function prepare(array $fixture)
    {
        foreach ($fixture as $table => $rows) {
            $this->table($table)->rows($rows);
        }
    }

    /**
     * @param null|array $tables array of table names to dump
     * @param bool $print print_r the dump or return it
     *
     * @return array|null
     */
    public function dump($tables = null, $print = false)
    {
        if (is_array($tables)) {
            $toDump = [];
            foreach ($tables as $table) {
                $toDump[$table] = $this->tables[$table];
            }
        }
        else {
            $toDump = $this->tables;
        }

        $dump = [];
        foreach ($toDump as $tableName => $table) {
            $dump[$tableName] = $table->dump();
        }

        if ($print) {
            print_r($dump);

            return null;
        }

        return $dump;
    }

    /**
     * Gets the table name part of the prototype name
     *
     * @param $prototypeName
     *
     * @return mixed
     */
    protected function getName($prototypeName)
    {
        $prototypeName = explode('#', $prototypeName);

        return $prototypeName[0];
    }

    /**
     *
     */
    public function tearDown()
    {
        foreach ($this->tables as $table) {
            $table->tearDown();
        }
    }
}
