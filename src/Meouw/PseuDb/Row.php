<?php

namespace Meouw\PseuDb;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use PHPUnit\Framework\Assert;

/**
 * Class Row
 */
class Row extends Assert
{
    /** @var array */
    protected $rowData;
    /** @var string */
    protected $tableName;
    /** @var Connection */
    protected $cxn;
    /** @var array */
    protected $primaryKey;
    /** @var string */
    protected $query;

    /**
     * Row constructor.
     * @param Connection $cxn
     * @param $tableName
     * @param array $primaryKey
     * @throws DBALException
     */
    public function __construct(Connection $cxn, $tableName, array $primaryKey)
    {
        $this->cxn = $cxn;
        $this->tableName = $tableName;
        $this->primaryKey = $primaryKey;
        $this->createQuery($tableName, $primaryKey);
        $this->reload();
    }

    /**
     * @param $tableName
     * @param $primaryKey
     */
    protected function createQuery($tableName, $primaryKey)
    {
        $q = $this->cxn->createQueryBuilder();
        $and = $q->expr()->andX();

        foreach ($primaryKey as $key => $value) {
            $and->add("$key = :$key");
        }

        $q->select('SQL_CALC_FOUND_ROWS *')->from($tableName, 't')->where($and);
        $this->query = $q->getSQL();
    }

    /**
     * @throws DBALException
     */
    public function reload()
    {
        $data = $this->cxn->fetchAssoc($this->query, $this->primaryKey);
        if ($data) {
            if ($this->cxn->fetchColumn('SELECT FOUND_ROWS()') > 1) {
                $this->fail('Row not unique, the key is not primary');
            }
            $this->rowData = $data;
        }
        else {
            $this->rowData = [];
            $this->fail('Row does not exist');
        }
    }

    /**
     * Asserts that the field equals $expected value
     *
     * @param string|int|float $expected
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldEquals($expected, $fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is equal to '$expected'";
        }
        $actual = $this->getActual($fieldName);
        static::assertEquals($expected, $actual, $message);

        return $this;
    }

    /**
     * Asserts that the field is null
     *
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldNull($fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is null";
        }
        $actual = $this->getActual($fieldName);
        static::assertNull($actual, $message);

        return $this;
    }

    /**
     * Asserts that the field is greater than the $expected value
     *
     * @param int|float $expected
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldGreaterThan($expected, $fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is greater than $expected";
        }
        $actual = $this->getActual($fieldName);
        static::assertGreaterThan($expected, $actual, $message);

        return $this;
    }

    /**
     * Asserts that the field is greater than or equal to the $expected value
     *
     * @param int|float $expected
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldGreaterThanOrEqual($expected, $fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is greater than or equal to $expected";
        }
        $actual = $this->getActual($fieldName);
        static::assertGreaterThanOrEqual($expected, $actual, $message);

        return $this;
    }

    /**
     * Asserts that the field is less than the $expected value
     *
     * @param int|float $expected
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldLessThan($expected, $fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is less than $expected";
        }
        $actual = $this->getActual($fieldName);
        static::assertLessThan($expected, $actual, $message);

        return $this;
    }

    /**
     * Asserts that the field is less than or equal to the $expected value
     *
     * @param int|float $expected
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldLessThanOrEqual($expected, $fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is less than or equal to $expected";
        }
        $actual = $this->getActual($fieldName);
        static::assertLessThanOrEqual($expected, $actual, $message);

        return $this;
    }

    /**
     * Asserts that the field is empty
     * Zero dates are considered empty
     *
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldEmpty($fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` is empty";
        }
        $actual = $this->getActual($fieldName);

        if ($actual == '0000-00-00 00:00:00' || $actual == '0000-00-00') {
            $actual = '';
        }
        static::assertEmpty($actual, $message);

        return $this;
    }

    /**
     * Asserts that the field matches $pattern
     *
     * @param string $pattern
     * @param string $fieldName
     * @param string|null $message
     *
     * @return Row
     */
    public function assertFieldRegExp($pattern, $fieldName, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$fieldName` matched '$pattern'";
        }
        $actual = $this->getActual($fieldName);
        static::assertRegExp($pattern, $actual, $message);

        return $this;
    }

    /**
     * @param $fieldName
     * @return mixed
     */
    private function getActual($fieldName)
    {
        if (!array_key_exists($fieldName, $this->rowData)) {
            static::fail("$fieldName does not exist in $this->tableName");
        }

        return $this->rowData[$fieldName];
    }
}
