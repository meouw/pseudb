<?php

namespace Meouw\PseuDb;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use LogicException;
use Meouw\PseuDb\Prototype\Reader;
use PHPUnit\Framework\Assert;

abstract class MockTable extends Assert
{
    /** @var Connection */
    protected $cxn;
    /** @var string */
    protected $tableName;
    /** @var array */
    protected $defaultData = [];
    /** @var bool */
    protected $readonly = true;

    /**
     * MockTable constructor.
     * @param Connection $cxn
     * @param $tableName
     * @param Reader $prototypeReader
     * @param DateTime $fixtureDate
     */
    public function __construct(Connection $cxn, $tableName, Reader $prototypeReader, DateTime $fixtureDate)
    {
        $this->cxn = $cxn;

        $variant = null;
        if (strpos($tableName, '#') !== false) {
            list($tableName, $variant) = explode('#', $tableName);
        }
        $this->tableName = $tableName;

        $data = $prototypeReader->read($tableName, clone $fixtureDate, $variant);

        if (is_null($data)) {
            $this->readonly = true;

            return;
        }

        $this->readonly = false;
        $this->defaultData = $data;
    }

    /**
     * @return mixed
     */
    abstract public function tearDown();

    /**
     * Truncates the table
     *
     * @return MockTable
     * @throws DBALException
     */
    public function truncate()
    {
        if ($this->readonly) {
            throw new LogicException("Table $this->tableName is not managed");
        }
        $this->cxn->exec('SET foreign_key_checks = 0');
        $this->cxn->executeQuery('TRUNCATE TABLE '.$this->tableName);
        $this->cxn->exec('SET foreign_key_checks = 1');

        return $this;
    }

    /**
     * Inserts the data you supply into the table
     * Any fields not specified will have their values set to the default read from the fixture
     *
     * @param array $data
     *
     * @return MockTable
     * @throws LogicException
     * @throws DBALException
     */
    public function row(array $data = [])
    {
        if ($this->readonly) {
            throw new LogicException("Table $this->tableName is not managed");
        }
        $data = array_merge($this->defaultData, $data);
        $this->cxn->insert($this->tableName, $data);

        return $this;
    }

    /**
     * Inserts multiple rows
     *
     * @param array|int $dataOrCount an array of rows or the number of rows to insert
     *
     * @return MockTable
     * @throws DBALException
     */
    public function rows($dataOrCount)
    {
        if ($this->readonly) {
            throw new LogicException("Table $this->tableName is not managed");
        }
        if (is_int($dataOrCount)) {
            while ($dataOrCount--) {
                $this->row();
            }

            return $this;
        }
        foreach ($dataOrCount as $row) {
            $this->row($row);
        }

        return $this;
    }

    /**
     * Returns an array of all data for this table or print_r's it
     *
     * @param bool $print
     *
     * @return array|null
     */
    public function dump($print = false)
    {
        $res = $this->cxn->fetchAll(
            'SELECT * FROM '.$this->tableName
        );
        if ($print) {
            print_r($res);

            return null;
        }

        return $res;
    }

    /**
     * Returns a refreshable snapshot of a row that can be used for assertions
     *
     * @param array $primaryKey
     *
     * @return null|Row
     * @throws DBALException
     */
    public function getRow(array $primaryKey)
    {
        return new Row($this->cxn, $this->tableName, $primaryKey);
    }

    /**
     * Asserts that this table has $expectedCount number of rows
     *
     * @param      $expectedCount
     * @param null $message
     *
     * @return MockTable
     * @throws DBALException
     */
    public function assertRowCount($expectedCount, $message = null)
    {
        if (is_null($message)) {
            $message = "Failed to assert that `$this->tableName` has $expectedCount rows";
        }
        $actualCount = $this->cxn->fetchColumn(
            'SELECT count(*) FROM '.$this->tableName
        );
        static::assertEquals($expectedCount, $actualCount, $message);

        return $this;
    }

    /**
     * Asserts that at least one row exists that matches the supplied parameters
     *
     * @param array $params
     * @param null $message
     *
     * @return MockTable
     * @throws DBALException
     */
    public function assertRowExists(array $params, $message = null)
    {
        if (is_null($message)) {
            $paramStr = [];
            foreach ($params as $key => $value) {
                $paramStr[] = "`$key` = '$value'";
            }
            $paramStr = implode(', ', $paramStr);
            $message = "Failed to assert that `$this->tableName` has a row with $paramStr";
        }

        $q = $this->cxn->createQueryBuilder();
        $and = $q->expr()->andX();

        foreach ($params as $key => $value) {
            $and->add("$key = :$key");
        }

        $q->select('COUNT(*)')->from($this->tableName, 't')->where($and);

        $res = (int)$this->cxn->fetchColumn($q->getSQL(), $params);
        static::assertGreaterThan(0, $res, $message);

        return $this;
    }

}
