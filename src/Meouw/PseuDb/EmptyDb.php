<?php
namespace Meouw\PseuDb;

class EmptyDb extends MockDb
{
    /**
     * @param $prototypeName
     *
     * @return MockTable
     */
    public function table($prototypeName)
    {
        return parent::createTable(EmptyTable::class, $prototypeName);
    }
}
